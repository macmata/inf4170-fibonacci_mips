            .data
newline:    .asciiz "\n"
len_fibo:   .word 20
            .text
main:       addi $s0,$zero,0    # init 0
            addi $s1,$s0,1      # init 1 
            addi $s2,$zero,0    # init counter

            add $a0,$zero,$s0   # move current value to arg
            jal println

            addi $s2,$s2,1      # increment counter
                                # do 
loop:       add $t2,$s0,$s1     #       sum 0 and 1th value
            add $a0,$zero,$t2   #       move value to arg0
            jal println         #       print value in arg0
            add $t0,$zero,$s0   #       move current  to temp
            add $s0,$zero,$t2   #       move sum to next value 
            add $s1,$zero,$t0   #       last value == current value
            addi $s2,$s2,1      #       incremnt counter
            lw $t1,len_fibo     # while  counter == len_fibo  
            bne $s2,$t1,loop    #   
            jal exit
            
# need to prvide integer to $a0
 println:   addi $v0,$zero,1    # load sycall print interger
            syscall             # call syscall
            la $a0, newline     # load in arg0 newline caractere
            addi $v0,$zero,4    # load syscall to print char
            syscall             # call syscall
            jr $ra              # branch to return address

# exit 
exit:       addi $v0,$zero,10   # load syscall to exit             
            syscall             # call syscall
